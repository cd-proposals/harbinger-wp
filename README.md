# Harbinger House WordPress Website

This repo contains the relevant code for the Harbinger House website.

The code is split up into a custom theme and custom plugins. All plugins should be prefixed with `harbinger-`.

## Plugins

Other plugins are ignored by Git, but at the time of writing the site uses each of the following additional plugins, some of which are required for the theme to work properly.

- Classic Editor (required)
- Advanced Custom Fields PRO (required)
- Custom Post Type UI (required)
- Safe SVG (required for safely uploading SVG images to the media library)
- Contact Form 7 (required for the contact form)

## Theme

**IMPORTANT:** The `harbinger` theme disables jQuery on the frontend, because at the time of writing we did not need it. Some plugins may rely on jQuery.

If you need to use jQuery, remove the `HARBINGER_DISABLE_JQUERY` constant defined in the theme's `functions.php`.

There are a few tools that are used to assist with development:

- PostCSS to compile all CSS and enable some future-facing syntax, and to minify the production build
- Preact to render the carousel on the homepage and work page, as well as the sticky scroller on the process page
- ESBuild to compile and minify the site's scripts

## Development

This project uses npm v8 workspaces. In the project root you can run `npm run dev` or `npm run build` to develop or build the site for production.
