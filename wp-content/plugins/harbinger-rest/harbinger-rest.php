<?php
/**
 * Harbinger REST API
 *
 * @package HARBINGER_REST
 * @author Chance
 * @license gplv2-or-later
 * @version 1.0.0
 *
 * @wordpress-plugin
 * Plugin Name: Harbinger REST API
 * Plugin URI: https://chance.dev
 * Description: REST API modifications for the Harbinger website.
 * Version: 1.0.0
 * Author: Chance
 * Author URI: https://chance.dev
 * Text Domain: harbinger-rest
 * License: GPLv2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 */

namespace Harbinger\REST;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

define( 'HARBINGER_REST_NAME', 'Harbinger REST API' );
define( 'HARBINGER_REST_VERSION', '1.0.0' );
define( 'HARBINGER_REST_PLUGIN_FILE', __FILE__ );
define( 'HARBINGER_REST_PLUGIN_BASE', plugin_basename( HARBINGER_REST_PLUGIN_FILE ) );
define( 'HARBINGER_REST_PLUGIN_DIR', plugin_dir_path( HARBINGER_REST_PLUGIN_FILE ) );
define( 'HARBINGER_REST_PLUGIN_URL', plugin_dir_url( HARBINGER_REST_PLUGIN_FILE ) );


\add_action( 'rest_api_init', __NAMESPACE__ . '\\register_projects_route' );
\add_action( 'rest_api_init', __NAMESPACE__ . '\\register_options_route' );

function register_projects_route() {
	\register_rest_route( 'harbinger/v1', '/projects(?:/(?P<id>\d+))?', [
		'methods'  => \WP_REST_Server::READABLE,
		'callback' => __NAMESPACE__ . '\\register_projects_route_callback',
		'args'     => [
			'id' => [
				'validate_callback' => function( $param, $request, $key ) {
					return is_numeric( $param );
				}
			],
		],
	] );
}

function register_projects_route_callback( \WP_REST_Request $request ) {
	$id = $request['id'];

	if ( isset( $id ) && !is_null( $id ) ) {
		$project = \get_post( $id );
		if ( !$project || $project->post_type !== 'project' ) {
			$response = new \WP_REST_Response( null );
			$response->set_status( 404 );
			return $response;
		}
		$project->featured_image = get_post_images( $id );
		$response = new \WP_REST_Response( $project );
		$response->set_status( 200 );
		return $response;
	}

	$query_params = $request->get_query_params();
	$posts_per_page = isset( $query_params['per_page'] ) && is_numeric( $query_params['per_page'] )
		? $query_params['per_page']
		: 10;

	$posts = \get_posts( [
		'post_type'      => 'project',
		'post_status'    => 'publish',
		'posts_per_page' => intval( $posts_per_page ),
	] );

	$posts = array_map( function ( $post ) {
		$post->featured_image = get_post_images( $post->ID );
		return $post;
	}, $posts );

	$response = new \WP_REST_Response( $posts );
	$response->set_status( 200 );
	return $response;
}

function register_options_route() {
	\register_rest_route( 'harbinger/v1', '/options(?:/(?P<id>[\w\-\_]+)/?(?P<field>[\w\-\_]+))?', [
		'methods'  => \WP_REST_Server::READABLE,
		'callback' => __NAMESPACE__ . '\\register_options_route_callback',
	] );
}

function register_options_route_callback( \WP_REST_Request $request ) {
	$id    = $request['id'];
	$field = $request['field'];

	if ( !isset( $id ) ) {
        $options  = [];
        $all_options  = \wp_load_alloptions( true );
        $last_key  = null;
        foreach ( $all_options as $option => $id ) {
            if ( str_starts_with( $option, '_options_' ) ) {
                $field = ltrim( $option, '_' );
                $value = \get_option( $field );
                // Unsure why the double ltrim is necessary, but it is. Trimming
                // from $field doesn't work right for some reason.
                $key = ltrim( ltrim( $field, 'options' ), '_' );

                /**
                 * ACF stores repeater options in a different format than
                 * others. Each value gets its own key in the options table
                 * formatted as `{repeater_field}_{row}_{sub_field}`. We want to
                 * return this as a proper array of objects.
                 *
                 * @example
                 * [
                 *     'repeater_field' => [
                 *         [ 'sub_field_0' => 'value' ],
                 *         [ 'sub_field_1' => 'value' ]
                 *     ]
                 * ]
                 */
                $regexp = '/^(' . $last_key . ')_(\d+)_(\S+)$/';
                if ( preg_match( $regexp, $key, $matches ) ) {
                    $field    = $matches[1];
                    $index    = intval( $matches[2] );
                    $sub_key  = $matches[3];
                    if ( is_string( $options[$field] ) || empty( $options[$field] ) ) {
                        $options[$field] = [];
                        $options[$field][$index] = [ $sub_key => $value ];
                    } elseif ( is_array( $options[$field] ) ) {
                        if ( is_array( $options[$field][$index] ) ) {
                            $options[$field][$index][$sub_key] = $value;
                        } else {
                            $options[$field][$index] = [ $sub_key => $value ];
                        }
                    }
                    $last_key = $field;
                } else {
                    $options[$key] = $value;
                    $last_key = $key;
                }
            }
        }

		$response = new \WP_REST_Response( $options );
		$response->set_status( 200 );
		return $response;
	}

    $options = \get_option( $id );

	if ( !isset( $field ) ) {
        if ( empty( $options ) ) {
            $response = new \WP_REST_Response( null );
            $response->set_status( 404 );
            return $response;
        }
		$response = new \WP_REST_Response( $options );
		$response->set_status( 200 );
		return $response;
	}

	if ( !isset( $options[$field] ) ) {
		$response = new \WP_REST_Response( null );
		$response->set_status( 404 );
		return $response;
	}

	$response = new \WP_REST_Response( $options[$field] );
	$response->set_status( 200 );
	return $response;
}

function get_post_images( $post_id ) {
	$sizes = \get_intermediate_image_sizes();
	$post_thumbnail_id = \get_post_thumbnail_id( $post_id );
	if ( !$post_thumbnail_id ) {
		return null;
	}

	$alt     = \get_post_meta( $post_thumbnail_id, '_wp_attachment_image_alt', true );
	$caption = \wp_get_attachment_caption( $post_thumbnail_id );
	$image   = [
		'id'      => $post_thumbnail_id,
		'alt'     => $alt,
		'caption' => $caption,
		'sizes'   => [],
	];

	foreach ( $sizes as $size ) {
		$img = \wp_get_attachment_image_src( $post_thumbnail_id, $size );
		if ( $img ) {
			$image['sizes'][$size] = [
				'url'     => $image[0],
				'width'   => $image[1],
				'height'  => $image[2],
			];
		} else {
			$image['sizes'][$size] = null;
		}
	}
	return $image;
}
