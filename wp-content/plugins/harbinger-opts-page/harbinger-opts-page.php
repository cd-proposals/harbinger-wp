<?php
/**
 * Harbinger Options Page
 *
 * @package HARBINGER_OPTS
 * @author Chance
 * @license gplv2-or-later
 * @version 1.0.0
 *
 * @wordpress-plugin
 * Plugin Name: Harbinger Options Page
 * Plugin URI: https://chance.dev
 * Description: Options page for Harbinger House website
 * Version: 1.0.0
 * Author: Chance
 * Author URI: https://chance.dev
 * Text Domain: harbinger-opts-page
 * License: GPLv2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

define( 'HARBINGER_OPTS_NAME', 'Harbinger Options Page' );
define( 'HARBINGER_OPTS_VERSION', '1.0.0' );
define( 'HARBINGER_OPTS_PLUGIN_FILE', __FILE__ );
define( 'HARBINGER_OPTS_PLUGIN_BASE', plugin_basename( HARBINGER_OPTS_PLUGIN_FILE ) );
define( 'HARBINGER_OPTS_PLUGIN_DIR', plugin_dir_path( HARBINGER_OPTS_PLUGIN_FILE ) );
define( 'HARBINGER_OPTS_PLUGIN_URL', plugin_dir_url( HARBINGER_OPTS_PLUGIN_FILE ) );

function harbinger_opts_main() {
	if ( function_exists( 'acf_add_options_page' ) ) {
		acf_add_options_page( [
			'page_title' 	=> 'Harbinger Site Settings',
			'menu_title'	=> 'Harbinger Site Settings',
			'menu_slug' 	=> 'site-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false,
            'autoload'      => true,
		] );
	}
}

harbinger_opts_main();
