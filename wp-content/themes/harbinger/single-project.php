<?php
$client_name         = get_field( 'client_name' );
$project_name        = get_field( 'project_name' );
$title               = get_the_title();
get_header( null, [ 'template' => 'project' ] );

function harbinger_get_embed_url( string $url ) {
    if ( str_contains( $url, 'player.vimeo.com' ) ) {
        return $url;
    } elseif ( str_contains( $url, 'vimeo.com' ) ) {
        $url = str_replace( 'vimeo.com/', 'player.vimeo.com/video/', $url );
        return $url;
    }
    return null;
}
?>

<main>
    <?php
    if ( have_posts() ) :
        while ( have_posts() ) :
            the_post();
            ?>
            <article class="container">
                <header>
                    <h1 itemprop="name" class="page--project__page-title">
                        <?php if ( $client_name ) : ?>
                            <span class="page--project__page-title-client"><?php echo esc_html( $client_name ) ?> </span>
                        <?php endif; ?>
                        <span class="page--project__page-title-project"><?php echo esc_html( $project_name ?: $title ) ?></span>
                    </h1>
                </header>

                <?php if ( have_rows( 'videos' ) ) : ?>
                    <div class="page--project__videos">
                        <?php
                        while ( have_rows( 'videos' ) ) :
                            the_row();
                            $embed_url = harbinger_get_embed_url( get_sub_field( 'video_url' ) );
                            if ( !isset( $embed_url ) ) :
                                ?>
                                <div>Invalid video URL</div>
                            <?php else : ?>
                                <div class="page--project__video">
                                    <iframe class="page--project__video-iframe" src="<?php echo esc_url( $embed_url ) ?>" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                                </div>
                            <?php
                            endif;
                        endwhile;
                        ?>
                    </div>
                <?php endif; ?>

                <?php
                $images       = get_field( 'image_gallery' );
                $preview_size = 'medium';
                $full_size    = 'full';

                $_width_default  = 3;
                $_height_default = 2;

                $image_aspect_ratio        = get_field( 'image_gallery_aspect_ratio' ) ?: '';
                $image_aspect_ratio_width  = trim( explode( ':', $image_aspect_ratio )[0] ?: '' ) ?: $_width_default;
                $image_aspect_ratio_height = trim( explode( ':', $image_aspect_ratio )[1] ?: '' ) ?: $_height_default;
                if ( ! is_numeric( $image_aspect_ratio_width ) ) {
                    $image_aspect_ratio_width = $_width_default;
                }
                if ( ! is_numeric( $image_aspect_ratio_height ) ) {
                    $image_aspect_ratio_width = $_height_default;
                }

                if ( $images ) :
                    ?>
                    <ul class="page--project__gallery" data-aspect-height="<?php echo esc_attr( $image_aspect_ratio_height ) ?>" style="--aspect-width:<?php echo esc_attr( $image_aspect_ratio_width ) ?>;--aspect-height:<?php echo esc_attr( $image_aspect_ratio_height ) ?>;">
                        <?php foreach ( $images as $image ) : ?>
                            <li class="page--project__gallery-item" data-aspect-ratio="<?php ?>">
                                <a class="page--project__gallery-link" data-fancybox="gallery" href="<?php echo esc_url( $image['url'] ) ?>"><img class="page--project__gallery-img" src="<?php echo esc_url( $image['sizes'][$preview_size] ) ?>" alt="<?php echo esc_attr( $image['alt'] ) ?>" /></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

                <?php if ( get_field( 'project_description' ) ) : ?>
                    <p><?php the_field( 'project_description' ) ?></p>
                <?php endif; ?>

                <?php if ( have_rows( 'credits' ) ) : ?>
                    <aside class="page--project__credits">
                        <h2 class="page--project__credits-title">Credits</h2>
                        <dl class="page--project__credits-list">
                            <?php
                            while ( have_rows( 'credits' ) ) :
                                the_row();
                                $credit_name = get_sub_field( 'credit' );
                                $job         = get_sub_field( 'job' );
                                ?>
                                <div class="page--project__credits-item">
                                    <dt class="page--project__credits-credit"><?php echo esc_html( $job ) ?></dt>
                                    <dd class="page--project__credits-job"><?php echo esc_html( $credit_name ) ?></dd>
                                </div>
                            <?php endwhile; ?>
                        </dl>
                    </aside>
                <?php endif; ?>

            </article>
            <?php
        endwhile;
    endif;
    ?>
</main>

<?php get_footer( null, [ 'template' => 'project' ] ) ?>
