<div class="<?php echo esc_attr( harbinger_class_names( "ui--start-project-cta", $args['class'] ) ) ?>">
    <div class="container">
        <a href="/contact#form" class="ui--start-project-cta__link">Start a Project</a>
    </div>
    <?php
    $i = -1;
    for ( $i = 0; $i < 4; $i++ ) :
        ?>
        <div class="<?php echo esc_attr( harbinger_class_names(
            "ui--start-project-cta__scroller",
            "ui--start-project-cta__scroller--hand",
            [
                "ui--start-project-cta__scroller--hand-flipped" => $i % 2 === 0,
            ]
        ) ) ?>" style="top: <?php echo $i * 150 ?>px; left: 0; right: 0;"></div>
        <div class="<?php echo esc_attr( harbinger_class_names(
            "ui--start-project-cta__scroller",
            "ui--start-project-cta__scroller--eye",
            [
                "ui--start-project-cta__scroller--eye-flipped" => $i % 2 === 0,
            ]
        ) ) ?>" style="top: <?php echo ($i + 1) * 150 - 31 ?>px; left: 50%; transform: translateX(calc(-50% + <?php echo $i * 200 ?>px));"></div>
    <?php endfor;  ?>
</div>
