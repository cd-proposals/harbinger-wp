
<?php
$class      = $args['class'];
$item_class = $args['item_class'];
$link_class = $args['link_class'];
$icon_class = $args['icon_class'];
if ( have_rows( 'links', 'option' ) ) :
	?>
	<ul class="<?php echo esc_attr( harbinger_class_names( "ui--social-menu", $class ) ) ?>">
		<?php
		while( have_rows( 'links', 'option' ) ) :
			the_row();
			$name = get_sub_field( 'name' );
			$url  = get_sub_field( 'url' );
			?>
			<li class="<?php echo esc_attr( harbinger_class_names( "ui--social-menu__item", $item_class ) ) ?>">
				<a class="<?php echo esc_attr( harbinger_class_names( "ui--social-menu__item-link", $link_class ) ) ?>" href="<?php echo esc_url( $url ) ?>" target="_blank" rel="noreferrer noopener">
					<span class="sr-only"><?php echo esc_html( $name ) ?></span>
					<?php harbinger_render_icon( $name, [
						'class' => harbinger_class_names(
							"ui--social-menu__item-icon",
							$icon_class
						),
					] ) ?>
				</a>
			</li>
		<? endwhile; ?>
	</ul>
<?php endif; ?>
