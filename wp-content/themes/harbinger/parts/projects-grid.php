<?php
$project_posts   = $args['project_posts'];
$uploads_baseurl = wp_upload_dir()['baseurl'];
?>

<div class="ui--projects-grid container">
    <div class="ui--projects-grid__grid">
        <?php
        foreach ( $project_posts as $post ) :
            setup_postdata( $post );
            $client_name     = get_field( 'client_name' );
            $project_name    = get_field( 'project_name' );
            $image_id        = get_post_thumbnail_id();
            $image_html      = '<img alt="" height="500" width="500" src="https://via.placeholder.com/500x500" />';
            $used_image_data = [
                'alt'   => '',
                'height'=> 500,
                'width' => 500,
                'url'   => 'https://via.placeholder.com/500x500',
            ];

            if ( $image_id ) {
                $image_data      = wp_get_attachment_metadata( $image_id );
                $prev_image_data = null;
                $prev_image_size = null;
                $used_image_data = null;
                $used_image_size = null;
                foreach ( $image_data['sizes'] as $size => $image_size_data ) {
                    if ( $prev_image_data && $prev_image_data['height'] > $image_size_data['height'] ) {
                        $used_image_data = $prev_image_data;
                        $used_image_size = $prev_image_size;
                    } else {
                        $used_image_data = $image_size_data;
                        $used_image_size = $size;
                    }
                    $prev_image_data = $image_size_data;
                    $prev_image_size = $size;
                }
                $used_image_data['alt'] = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
                $used_image_data['url'] = wp_get_attachment_image_url( $image_id, $used_image_size );
            }
            $title = $client_name . ": " . $project_name;
            ?>
            <figure class="ui--projects-grid__item">
                <a class="ui--projects-grid__item-link" href="<?php echo esc_url( the_permalink() ) ?>" aria-label="See project: <?php echo esc_attr( $project_name ) ?>">
                    <div class="ui--projects-grid__item-image" role="img"
                  aria-label="<?php echo esc_attr( $used_image_data['alt'] ?: "Design for " . $project_name ) ?>" style="background-image: url(<?php echo esc_url( $used_image_data['url'] ) ?>);"></div>
                </a>
                <figcaption class="ui--projects-grid__item-caption">
                    <a tabindex="-1" class="ui--projects-grid__caption-link" href="<?php echo esc_url( the_permalink() ) ?>">
                        <?php echo esc_html( $title ) ?></figcaption>
                    </a>
            </figure>
        <?php endforeach; ?>
    </div>
</div>
