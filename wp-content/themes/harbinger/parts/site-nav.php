<?php
$class                = $args['class'];
$dialog_overlay_class = $args['dialog_overlay_class'];
$close_button_class   = $args['close_button_class'];
$current_page_link    = get_permalink( get_the_ID() );
?>

<div id="site-nav-dialog" data-a11y-dialog-container aria-label="Site Navigation" aria-hidden="true">
    <div tabindex="-1" data-a11y-dialog-hide data-a11y-dialog-overlay=""></div>
    <div id="#site-nav-root" class="ui--site-nav ui--site-nav ui--dialog__overlay">
        <!-- 3. The actual dialog -->
        <div role="document" data-a11y-dialog-content class="<?php echo esc_attr(
            harbinger_class_names(
                'ui--site-nav__inner',
                'ui--dialog__content',
                'ui--site-nav__inner--open',
                $class
            ) ) ?>">
            <div class="site-header-stub">
                <div class="site-header-stub__logo"></div>
                <?php get_template_part( 'parts/hamburger-button', null, [
                    'class' => 'ui--site-nav__close-button ' . $close_button_class,
                    'label' => 'Close Nav Menu',
                    'attributes' => [
                        'data-a11y-dialog-hide' => 'site-nav-dialog',
                        'id'                    => 'site-nav-close-trigger',
                    ],
                ] ) ?>
            </div>
            <nav aria-label="Main" class="ui--site-nav__nav">
                <?php
                wp_nav_menu( [
                    'theme_location' => 'mainz',
                    'menu_class'     => 'ui--site-nav__nav-menu',
                ] );
                ?>
            </nav>
        </div>
    </div>
</div>
