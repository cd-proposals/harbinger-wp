<?php
$CLASS_NAME_ROOT = "ui--hamburger-button";
$CLASS_NAME_CROSS = "{$CLASS_NAME_ROOT}__cross";
$CLASS_NAME_CROSS_LINE = "{$CLASS_NAME_ROOT}__cross-line";
$CLASS_NAME_LINES = "{$CLASS_NAME_ROOT}__lines";
$CLASS_NAME_LINE = "{$CLASS_NAME_ROOT}__line";

$other_classes = $args['class'];
$label         = $args['label'];
$attributes    = $args['attributes'];
$attrs_str     = '';
if ( $attributes ) {
    $attrs_str = array_reduce(
        array_keys( $attributes ),
        function ( $carry, $attr ) use ( $attributes ) {
            return $carry . ' ' . esc_html( $attr ) . '="' . esc_attr( $attributes[$attr] ) . '"';
        },
        ''
    );
}
?>
<button data-ui-hamburger="" class="<?php echo esc_attr( harbinger_class_names( $CLASS_NAME_ROOT, $other_classes ) ) ?>" type="button" <?php echo $attrs_str ?>>
    <span class="<?php echo $CLASS_NAME_LINES ?>">
        <span class="<?php echo $CLASS_NAME_LINE ?>"></span>
        <span class="<?php echo $CLASS_NAME_LINE ?>"></span>
        <span class="<?php echo $CLASS_NAME_LINE ?>"></span>
    </span>
    <div class="<?php echo $CLASS_NAME_CROSS ?>">
        <span class="<?php echo $CLASS_NAME_CROSS_LINE ?>"></span>
        <span class="<?php echo $CLASS_NAME_CROSS_LINE ?>"></span>
    </div>
    <span class="sr-only"><?php echo esc_html( $label ) ?></span>
</button>
