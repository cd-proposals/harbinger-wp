<header class="<?php echo esc_attr( harbinger_class_names( "site-header", $args['class'] ) ) ?>">
	<?php the_custom_logo() ?>
	<?php get_template_part( 'parts/hamburger-button', null, [
		'class' => 'site-header__nav-button-open',
		'label' => 'Open Nav Menu',
		'attributes' => [
			'data-a11y-dialog-show' => 'site-nav-dialog',
			'id'     => 'site-nav-open-trigger',
		]
	] ) ?>

</header>
