<?php
$class = $args['class'];
$logo_class = $args['logo_class'];
$client_logos = get_field( 'client_logos', 'option' );

function harbinger_get_client_logo_img( array $props, string $logo_class ) {
    $name       = $props['client_name'];
    $logo_data  = $props['logo'];
    $alt        = $logo_data['alt'] ?: $name;
    $attributes = [ 'class' => $logo_class ];
    $url        = $logo_data['url'];
    $attrs_str  = array_reduce(
        array_keys( $attributes ),
		function ( $carry, $attr ) use ( $attributes ) {
			return $carry . ' ' . esc_html( $attr ) . '="' . esc_attr( $attributes[$attr] ) . '"';
		},
		''
	);
    return '<img src="' . esc_attr( $url ) . '" alt="' . esc_attr( $alt ) . '" data-logo="' . harbinger_pascal_case( esc_attr( $name ) ) . '" ' . $attrs_str . '/>';
}
?>

<div class="<?php echo esc_attr( harbinger_class_names( 'container', $class ) ) ?>">
    <?php foreach ( $client_logos as $logo ) : ?>
        <?php echo harbinger_get_client_logo_img( $logo, $logo_class ); ?>
    <?php endforeach ?>
</div>
