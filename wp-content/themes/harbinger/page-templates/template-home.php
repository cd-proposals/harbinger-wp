<?php
/**
 * Template Name: Home Page
 *
 * @package    WordPress
 * @subpackage Harbinger
 */

get_header( null, [ 'template' => 'home' ] );
?>

<main>
    <h1 class="sr-only">Harbinger House Commercial Production Studio</h1>
    <div class="page--home__page-title" aria-hidden="true">Commercial Production Studio</div>
    <section class="page--home__projects">
        <h2 class="sr-only">Featured Projects</h2>
        <?php get_template_part( 'parts/projects-carousel' ); ?>
    </section>

    <div data-remove="TODO" class="view-all">
        <div>
            <div>
                <a href="/work">
                    <span>View all work.</span>
                </a>
            </div>
        </div>
    </div>

    <section class="page--home__overview">
        <h2 class="sr-only">About Us</h2>
        <div>
            <?php the_content() ?>
        </div>
    </section>

</main>

<aside aria-label="Request a project">

    <?php get_template_part( 'parts/start-project-cta' ); ?>

    <section class="page--home__clients">
        <h2 class="sr-only">Clients</h2>
        <?php get_template_part( 'parts/client-logos', null, [
            'class'      => 'page--home__client-logos',
            'logo_class' => 'page--home__client-logo'
        ] ); ?>
    </section>

</aside>

<?php get_footer( null, [ 'template' => 'home' ] ); ?>
