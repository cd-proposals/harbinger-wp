<?php
/**
 * Template Name: Work Page
 *
 * @package    WordPress
 * @subpackage Harbinger
 */

get_header( null, [ 'template' => 'work' ] );
$project_posts    = get_posts( [
    'numberposts' => 20,
    'post_type'   => 'project',
] );
$tabs_id       = 'work-project-tabs';
$tabs_tab_id   = "{$tabs_id}--tab";
$tabs_panel_id = "{$tabs_id}--panel";
$tab_views     = [ 'Grid', 'Slides' ];
?>

<main>
    <?php
    while ( have_posts() ) :
        the_post();
        ?>

        <div id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="container">
                <?php the_title( '<h1 class="page-title" itemprop="name">', '</h1>' ); ?>
            </header>
            <?php if ( get_the_content() ) : ?>
                <div class="page--work__main-content" itemprop="mainContentOfPage">
                    <?php
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail( 'full', [ 'itemprop' => 'image' ] );
                    }
                    the_content();
                    ?>
                </div>
            <?php endif; ?>

            <div data-ui-tabs="" class="ui--tabs" data-orientation="horizontal" id="<?php echo esc_attr( $tabs_id ); ?>">
                <span class="sr-only">View: </span>
                <div role="tablist" aria-orientation="horizontal" class="ui--tabs__list" data-ui-tab-list="">

                    <?php
                    $i = -1;
                    foreach ( $tab_views as $tab_view ) :
                        $i++;
                        $selected = $i === 0;
                        ?>

                        <button aria-controls="<?php echo esc_attr( $tabs_panel_id . '--' . $i ); ?>" role="tab" aria-selected="<?php echo $selected ?>" tabindex="<?php echo $selected ? 0 : -1  ?>" class="ui--tabs__tab" data-ui-tab="" data-orientation="horizontal" <?php echo $selected ? 'data-selected=""' : "" ?> id="<?php echo esc_attr( $tabs_tab_id . '--' . $i ); ?>" type="button"><?php echo esc_html( $tab_view ); ?></button>

                    <?php endforeach; ?>

                </div>

                <div class="ui--tabs__panels" data-ui-tab-panels="">

                    <?php
                    $i = -1;
                    foreach ( $tab_views as $tab_view ) :
                        $i++;
                        $selected = $i === 0;
                        $hidden = !$selected;
                        ?>

                        <div aria-labelledby="<?php echo esc_attr( $tabs_tab_id . '--' . $i ); ?>" id="<?php echo esc_attr( $tabs_panel_id . '--' . $i ); ?>" role="tabpanel" tabindex="<?php echo $selected ? 0 : -1  ?>" class="ui--tabs__panel" data-ui-tab-panel="" data-orientation="horizontal" <?php echo $selected ? 'data-selected=""' : 'hidden="true"' ?>>
                            <?php
                            if ( $tab_view === 'Grid' ) {
                                get_template_part( 'parts/projects-grid', null, [
                                    'project_posts' => $project_posts,
                                ] );
                            } elseif ( $tab_view === 'Slides' ) {
                                get_template_part( 'parts/projects-carousel' );
                            }
                            ?>
                        </div>

                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    <?php endwhile ?>
</main>

<aside aria-label="Request a project">
    <?php get_template_part( 'parts/start-project-cta' ); ?>
</aside>

<?php get_footer( null, [ 'template' => 'work' ] ); ?>
