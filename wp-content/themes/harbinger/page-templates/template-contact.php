<?php
/**
 * Template Name: Contact Page
 *
 * @package    WordPress
 * @subpackage Harbinger
 */

$email           = get_field( 'email', 'option' );
$phone_number    = get_field( 'phone_number', 'option' );
$phone_raw       = preg_replace( '/[\.\-()]/', '', $phone_number ?: '' );
$mailing_address = get_field( 'mailing_address', 'option' );
$map_url         = get_field( 'map_url', 'option' );
$year            = esc_html( date_i18n( __( 'Y', 'harbinger' ) ) );

$fields = [
    [
        'name'       => "name",
        'label'      => "My name is",
        'aria-label' => "Full name",
        'required'   => true,
        'type'       => "text"
    ],
    [
        'name'       => "topic",
        'label'      => "I'd like to chat about",
        'aria-label' => "Inquiry topic",
        'required'   => false,
        'type'       => "select",
        'options'    => [
            [ 'name' => 'Video Project', 'value' => 'video-project' ],
            [ 'name' => 'Animation', 'value' => 'animation' ],
            [ 'name' => 'Just wanted to connect', 'value' => 'general' ],
        ],
    ],
    [
        'name'       => 'company',
        'label'      => 'My company/brand is',
        'aria-label' => 'Company',
        'required'   => true,
        'type'       => 'text',
    ],
    [
        'name'       => 'email',
        'label'      => 'My email address is',
        'aria-label' => 'Email',
        'required'   => true,
        'type'       => 'email',
    ],
    [
        'name'       => 'phone',
        'label'      => 'My phone number is',
        "aria-label" => 'Phone number',
        'required'   => true,
        'type'       => 'tel',
    ],
    [
        'name'       => "other",
        'label'      => "Thoughts on my project",
        'aria-label' => "Thoughts on my project",
        'required'   => false,
        'type'       => "textarea",
    ],
];

get_header( null, [ 'template' => 'contact' ] );
?>

<main>
    <div class="container">
        <h1 class="page-title">Say Hello</h1>
    </div>
    <div>
        <section class="page--contact__content-section">
            <div class="container">
                <div class="page--contact__content">
                    <div class="page--contact__row">
                        <ul class="page--contact__points">
                            <li>
                                <a class="page--contact__form-link" href="#form">
                                    New project form
                                </a>
                            </li>
                            <?php if ( $phone_number ) : ?>
                                <li>
                                    <a href="tel:+1<?php echo esc_attr( $phone_raw ) ?>" aria-label="Call <?php echo esc_html( $phone_number ) ?>"><?php echo esc_html( $phone_number ) ?></a>
                                </li>
                            <?php endif; ?>
                            <?php if ( $email ) : ?>
                                <li>
                                    <a href="mailto:<?php echo esc_attr( $email ) ?>"><?php echo esc_html( $email ) ?></a>
                                </li>
                            <?php endif; ?>
                        </ul>
                        <div class="ui--address">
                            <h4 class="ui--address__heading">Our Home:</h4>
                            <span>
                                <?php echo $mailing_address ?>
                            </span>
                            <div>
                                <?php if ( $map_url ) : ?>
                                    <a class="ui--address__map-link" href="<?php echo esc_url( $map_url ) ?>" aria-label="Map to address">Map</a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <div class="page--contact__social-wrapper">
                        <h2 class="page--contact__social-heading">Follow us:</h2>
                        <?php
                        get_template_part( 'parts/social-menu', null, [
                            'class'      => 'page--contact__social-nav',
                            'item_class' => 'page--contact__social-nav-item',
                            'link_class' => 'page--contact__social-nav-link',
                            'icon_class' => 'page--contact__social-nav-icon',
                        ] );
                        ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>

<aside class="page--contact__form-section" id="form" aria-label="Request a project">
    <div class="container">
        <h2 class="page--contact__form-heading">Hello!</h2>
        <?php echo do_shortcode( get_field( 'contact_form_shortcode' ) ) ?>
    </div>
</aside>

<?php get_footer( null, [ 'template' => 'contact' ] ); ?>
