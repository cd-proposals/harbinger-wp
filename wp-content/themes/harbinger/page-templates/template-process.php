<?php
/**
 * Template Name: Process Page
 *
 * @package    WordPress
 * @subpackage Harbinger
 */

get_header( null, [ 'template' => 'process' ] );
?>

<main class="container">
    <h1 class="page-title">Our Process</h1>
    <div id="process-root"></div>
</main>

<aside aria-label="Request a project">
    <?php get_template_part( 'parts/start-project-cta' ); ?>
</aside>

<?php get_footer( null, [ 'template' => 'process' ] ); ?>
