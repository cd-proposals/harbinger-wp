<?php get_header( null, [ 'template' => 'post' ] ) ?>

<main>
    <?php
    if ( have_posts() ) :
        while ( have_posts() ) :
            the_post();
            ?>
            <article class="container">
                <header>
                    <?php the_title( '<h1 itemprop="name" class="page--post__page-title">', '</h1>' ); ?>
                </header>
                <div class="page--post__page-content">
                    <?php the_content(); ?>
                </div>
            </article>
            <?php
        endwhile;
    endif;
    ?>
</main>

<?php get_footer( null, [ 'template' => 'post' ] ) ?>
