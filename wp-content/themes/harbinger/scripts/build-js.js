const esbuild = require("esbuild");

const args = process.argv.slice(2);

// Promise.all(
//   ["js/scripts.js", "js/projects-carousel.js", "js/process.js"].map(build)
// ).then(() => {
//   console.log("Build complete");
// });

build();

async function build() {
  await esbuild.build({
    entryPoints: [
      "js/scripts.js",
      "js/project-gallery.js",
      "js/projects.js",
      "js/projects-carousel.jsx",
      "js/process.jsx",
    ],
    bundle: true,
    minify: true,
    sourcemap: true,
    jsxFactory: "h",
    jsxFragment: "Fragment",
    target: ["chrome58", "firefox57", "safari11", "edge18"],
    outdir: "dist/js",
    watch: args.includes("--watch")
      ? {
          onRebuild(error) {
            if (error) {
              console.error("Build failed:", error);
            } else {
              console.log("Build succeeded");
            }
          },
        }
      : false,
  });
}
