<?php get_header( null, [ 'template' => 'archive' ] ) ?>

<main class="container">
    <?php
    if ( have_posts() ) :
        while ( have_posts() ) :
            the_post();
            ?>
            <article class="page--archive__post">
                <header>
                    <?php the_title( '<h1 itemprop="name" class="page--archive__page-title">', '</h1>' ); ?>
                </header>
                <div class="page--archive__page-content">
                    <p><?php the_excerpt(); ?></p>
                    <div>
                        <a href="<?php the_permalink(); ?>" class="page--archive__more-link">Read more<span class="sr-only"> about <?php the_title() ?></span></a>
                    </div>
                </div>
            </article>
        <?php
        endwhile;
    endif;
    ?>
</main>

<?php get_footer( null, [ 'template' => 'archive' ] ) ?>
