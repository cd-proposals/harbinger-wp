const plugins = {
  "postcss-import": {},
  "postcss-preset-env": {
    stage: 2,
    features: {
      "logical-properties-and-values": {
        preserve: true,
      },
      "nesting-rules": true,
      "custom-media-queries": true,
    },
  },
  "postcss-100vh-fix": {},
};

if (process.env.NODE_ENV === "production") {
  plugins["cssnano"] = {};
}

module.exports = {
  plugins,
};
