<?php get_header( null, [ 'template' => 'index' ] ) ?>

<main>
    <?php
    if ( have_posts() ) :
        while ( have_posts() ) :
            the_post();
            ?>
            <article class="container">
                <header>
                    <?php the_title( '<h1 itemprop="name" class="page--index__page-title">', '</h1>' ); ?>
                </header>
                <div class="page--index__page-content">
                    <?php the_content(); ?>
                </div>
            </article>
        <?php
        endwhile;
    endif;
    ?>
</main>

<?php get_footer( null, [ 'template' => 'index' ] ) ?>
