<?php

// If you add any plugins to the site that rely on jQuery, you will need to
// re-enable this. This is an optimization since we don't use jQuery in the
// theme on the frontend.
if ( ! defined( 'HARBINGER_DISABLE_JQUERY' ) ) {
	define( 'HARBINGER_DISABLE_JQUERY', true );
}

add_action( 'after_setup_theme', 'harbinger_setup' );
add_action( 'init', 'harbinger_disable_emojis' );
add_action( 'wp_enqueue_scripts', 'harbinger_enqueue' );
add_action( 'wp_default_scripts', 'harbinger_remove_unused_scripts' );
add_filter( 'script_loader_tag', 'harbinger_deferred_scripts', 10, 2 );

// add_filter( 'script_loader_tag', 'harbinger_module_scripts' , 10, 3 );
add_filter( 'get_custom_logo', 'harbinger_custom_logo_markup' );
add_filter( 'document_title_separator', 'harbinger_document_title_separator' );
add_filter( 'the_title', 'harbinger_title' );
add_filter( 'nav_menu_link_attributes', 'harbinger_schema_url', 10 );

add_filter( 'wpcf7_autop_or_not', '__return_false' );
// if ( function_exists( 'wpcf7_add_form_tag' ) ) {
// 	add_action( 'wpcf7_init', 'harbinger_cf7_text_tag' );
// }

function harbinger_is_dev() {
	return defined( 'DEV_ENV' ) ? DEV_ENV : false;
}

function harbinger_jquery_is_disabled() {
	return defined( 'HARBINGER_DISABLE_JQUERY' ) ? HARBINGER_DISABLE_JQUERY : false;
}

function harbinger_get_serialized_process_blocks() {
	$page = get_page_by_path( 'process' );
	if ( !$page ) {
		return null;
	}

	$custom_fields = get_fields( $page->ID );
	$steps         = $custom_fields['steps'];
	$blocks        = [];
	foreach ( $steps as $step ) {
		$sub_steps_src = $step['sub_steps'];
		$sub_steps     = [];
		if ( isset( $sub_steps_src ) && !!$sub_steps_src ) {
			$i = -1;
			foreach ( $sub_steps_src as $sub_step ) {
				$i++;
				$sub_steps[] = [
					'stepTitle'   => $sub_step['sub_step_title'] ?: null,
					'description' => $sub_step['description'] ? wp_strip_all_tags( $sub_step['description'] ) : null,
				];
			}
		}
		$blocks[] = [
			'stepTitle' => $step['step_title'],
			'subSteps'  => $sub_steps,
		];
	}
	return json_encode( $blocks );
}

function harbinger_get_serialized_project_posts() {
	$project_posts    = get_posts( [
		'numberposts' => 20,
		'post_type'   => 'project',
	] ) ?: [];
	$modeled_projects = [];

	foreach ( $project_posts as $post ) {
		$custom_fields = get_fields( $post->ID );

		$videos = null;
		if ( $custom_fields['videos'] ) {
			$videos = array_map( function ( $video ) {
				return [
					'url'   => $video['video_url'] ?: null,
					'name'  => $video['video_name'] ?: null,
					'image' => $video['video_image'] ?: null,
				];
			}, $custom_fields['videos'] );
		}

		$credits = null;
		if ( isset( $custom_fields['credits'] ) && !!$custom_fields['credits'] ) {
			$credits = array_map( function ( $credit ) {
				return [
					'job'    => $credit['job'] ?: null,
					'credit' => $credit['credit'] ?: null,
				];
			}, $custom_fields['credits'] );
		}

		$thumb_id       = get_post_thumbnail_id( $post->ID );
		$featured_image = null;
		if ( isset( $thumb_id ) ) {
			$size_names = get_intermediate_image_sizes();
			$sizes = [];
			foreach ( $size_names as $size ) {
				$src = wp_get_attachment_image_src( $thumb_id, $size );
				$sizes[$size] = [
					'url'    => $src[0],
					'width'  => $src[1],
					'height' => $src[2],
				];
			}
			$featured_image = [
				'id'      => $thumb_id,
				'alt'     => get_post_meta( $thumb_id, '_wp_attachment_image_alt', true ) ?: null,
				'caption' => wp_get_attachment_caption( $thumb_id ) ?: null,
				'sizes'   => $sizes,
			];
		}

		$project = [
			'datePublished'      => $post->post_date,
			'guid'               => $post->guid,
			'id'                 => $post->ID,
			'slug'               => $post->post_name,
			'status'             => $post->post_status,
			'title'              => $post->post_title,
			'clientName'         => $custom_fields['client_name'] ?: null,
			'projectName'        => $custom_fields['project_name'] ?: $post->post_title,
			'projectDescription' => $custom_fields['project_description'] ?: null,
			'videos'             => $videos,
			'credits'            => $credits,
			'featuredImage'      => $featured_image,
		];

		$modeled_projects[] = $project;
	}
	return json_encode( $modeled_projects );
}

function harbinger_setup() {
	load_theme_textdomain( 'harbinger', get_template_directory() . '/languages' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'custom-logo', [
		'unlink-homepage-logo' => true,
		'width'                => 150,
		'height'               => 43,
		'flex-height'          => true,
		'flex-width'           => true,
	] );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'responsive-embeds' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'html5', [ 'search-form', 'navigation-widgets' ] );
	register_nav_menus( [
		'mainz' => esc_html__( 'Main Menu', 'harbinger' ),
	] );
}

function harbinger_custom_logo_markup( string $html ) {
	$html = str_replace( 'custom-logo-link', 'site-header__logo-link', $html );
	$html = str_replace( 'custom-logo', 'site-header__logo', $html );
	return $html;
}

/**
 * @param WP_Scripts $scripts
 * @return void
 */
function harbinger_remove_unused_scripts( $scripts ) {
	if ( harbinger_jquery_is_disabled() && ! is_admin() ) {
		$scripts->remove( 'jquery' );
	}
	$scripts->remove( 'wp-emoji-release' );
}

/**
 * Add an aysnc attribute to an enqueued script.
 *
 * @param  string  $tag    Tag for the enqueued script.
 * @param  string  $handle The script's registered handle.
 * @return string          Script tag for the enqueued script
 */
function harbinger_deferred_scripts( $tag, $handle ) {
	if ( 'harbinger-project-gallery' === $handle ) {
		return str_replace( ' src=', ' defer src=', $tag );
	}
	return $tag;
}

function harbinger_disable_emojis() {
	if ( is_admin() ) {
		return;
	}
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'harbinger_tinymce_plugins' );
}

function harbinger_tinymce_plugins( $plugins ) {
	var_dump($plugins);
	return array_diff( $plugins, [ 'wpemoji' ] );
	if ( is_array( $plugins ) ) {
		if ( ! is_admin() ) {
		}
	} else {
		return [];
	}
}

function harbinger_enqueue() {
	$disable_jquery = harbinger_jquery_is_disabled();
	$ver = harbinger_is_dev() ? false : '1.0.10';
	wp_enqueue_style( 'harbinger-style', get_stylesheet_uri() );
	wp_enqueue_style( 'harbinger-css', get_template_directory_uri() . '/dist/css/globals.css', [], $ver );
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css2?family=Assistant:wght@400;700;800&display=swap' );
	wp_enqueue_script( 'harbinger-script', get_template_directory_uri() . '/dist/js/scripts.js', [], $ver, false );

	if ( ! $disable_jquery ) {
		wp_enqueue_script( 'jquery' );
	}

	if ( is_page_template( 'page-templates/template-work.php' ) || is_front_page() ) {
		wp_enqueue_script( 'harbinger-projects-carousel', get_template_directory_uri() . '/dist/js/projects-carousel.js', [], $ver );
		wp_add_inline_script( 'harbinger-projects-carousel', 'window.__harbinger_projects = window.__harbinger_projects || ' . harbinger_get_serialized_project_posts() . ';', 'before' );
	}

	if ( is_page_template( 'page-templates/template-work.php' ) ) {
		wp_enqueue_script( 'harbinger-projects', get_template_directory_uri() . '/dist/js/projects.js', [], $ver );
	}

	if ( is_page_template( 'page-templates/template-process.php' ) ) {
		wp_enqueue_script( 'harbinger-process', get_template_directory_uri() . '/dist/js/process.js', [], $ver );
		wp_add_inline_script( 'harbinger-process', 'window.__harbinger_process_blocks = window.__harbinger_process_blocks || ' . harbinger_get_serialized_process_blocks() . ';', 'before' );
	}

	if ( is_singular( 'project' ) ) {
		wp_enqueue_script( 'harbinger-project-gallery', get_template_directory_uri() . '/dist/js/project-gallery.js', [], $ver );
	}
}

function harbinger_module_scripts( $tag, $handle, $src ) {
	if ( ! in_array( $handle, [
		'harbinger-projects-carousel',
		'harbinger-process',
	] ) ) {
		return $tag;
	}
	$tag = str_replace( 'text/javascript', 'module', $tag );
	return $tag;
}

function harbinger_document_title_separator( $sep ) {
	$sep = '|';
	return $sep;
}

function harbinger_title( $title ) {
	if ( $title == '' ) {
		return '...';
	} else {
		return $title;
	}
}

function harbinger_schema_type() {
	$schema = 'https://schema.org/';
	if ( is_single() ) {
		$type = "Article";
	} elseif ( is_author() ) {
		$type = 'ProfilePage';
	} elseif ( is_search() ) {
		$type = 'SearchResultsPage';
	} else {
		$type = 'WebPage';
	}
	echo 'itemscope itemtype="' . $schema . $type . '"';
}

function harbinger_schema_url( $atts ) {
	$atts['itemprop'] = 'url';
	return $atts;
}

function harbinger_wp_body_open() {
	do_action( 'wp_body_open' );
}

function harbinger_class_names() {
	$args = func_get_args();
	$data = array_reduce( $args, function ($carry, $arg) {
		if ( is_array( $arg ) ) {
			return array_merge($carry, $arg);
		}
		$carry[] = $arg;
		return $carry;
	}, [] );

	$classes = array_map( function ( $key, $value ) {
		$condition = $value;
		$return = $key;
		if ( is_int( $key ) ) {
			$condition = null;
			$return = $value;
		}

		$is_array = is_array( $return );
		$is_object = is_object( $return );
		$is_stringable_type = !$is_array && !$is_object;
		$is_stringable_object = $is_object && method_exists( $return, '__toString' );
		if (!$is_stringable_type && !$is_stringable_object) {
			return null;
		}
		if ($condition === null) {
			return $return;
		}
		return $condition ? $return : null;
	}, array_keys( $data ), array_values( $data ) );

	$classes = array_filter( $classes );
	return implode( ' ', $classes );
}

function harbinger_get_top_link_anchor_id() {
	return 'top-link-27fh62n8rtovja673b0';
}

function harbinger_render_icon( string $name, array $attributes = [] ) {
	$name = strtolower( $name );
	if ( !in_array( $name, [ 'facebook', 'vimeo', 'instagram', 'linkedin' ] ) ) {
		return;
	}
	$attrs_str = array_reduce(
		array_keys( $attributes ),
		function ( $carry, $attr ) use ( $attributes ) {
			return $carry . ' ' . esc_html( $attr ) . '="' . esc_attr( $attributes[$attr] ) . '"';
		},
		''
	);
	?>
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" <?php echo $attrs_str ?>>
	<?php
	switch ( $name ) {
		case 'facebook':
			?>
			<path d="M9 8h-3v4h3v12h5v-12h3.642l.358-4h-4v-1.667c0-.955.192-1.333 1.115-1.333h2.885v-5h-3.808c-3.596 0-5.192 1.583-5.192 4.615v3.385z"></path>
			<?php
			break;
		case 'linkedin':
			?>
			<path d="M4.98 3.5c0 1.381-1.11 2.5-2.48 2.5s-2.48-1.119-2.48-2.5c0-1.38 1.11-2.5 2.48-2.5s2.48 1.12 2.48 2.5zm.02 4.5h-5v16h5v-16zm7.982 0h-4.968v16h4.969v-8.399c0-4.67 6.029-5.052 6.029 0v8.399h4.988v-10.131c0-7.88-8.922-7.593-11.018-3.714v-2.155z"></path>
			<?php
			break;
		case 'vimeo':
			?>
			<path d="M22.875 10.063c-2.442 5.217-8.337 12.319-12.063 12.319-3.672 0-4.203-7.831-6.208-13.043-.987-2.565-1.624-1.976-3.474-.681l-1.128-1.455c2.698-2.372 5.398-5.127 7.057-5.28 1.868-.179 3.018 1.098 3.448 3.832.568 3.593 1.362 9.17 2.748 9.17 1.08 0 3.741-4.424 3.878-6.006.243-2.316-1.703-2.386-3.392-1.663 2.673-8.754 13.793-7.142 9.134 2.807z"></path>
			<?php
			break;
		case 'instagram':
			?>
			<path d="M12 2.163c3.204 0 3.584.012 4.85.07 3.252.148 4.771 1.691 4.919 4.919.058 1.265.069 1.645.069 4.849 0 3.205-.012 3.584-.069 4.849-.149 3.225-1.664 4.771-4.919 4.919-1.266.058-1.644.07-4.85.07-3.204 0-3.584-.012-4.849-.07-3.26-.149-4.771-1.699-4.919-4.92-.058-1.265-.07-1.644-.07-4.849 0-3.204.013-3.583.07-4.849.149-3.227 1.664-4.771 4.919-4.919 1.266-.057 1.645-.069 4.849-.069zm0-2.163c-3.259 0-3.667.014-4.947.072-4.358.2-6.78 2.618-6.98 6.98-.059 1.281-.073 1.689-.073 4.948 0 3.259.014 3.668.072 4.948.2 4.358 2.618 6.78 6.98 6.98 1.281.058 1.689.072 4.948.072 3.259 0 3.668-.014 4.948-.072 4.354-.2 6.782-2.618 6.979-6.98.059-1.28.073-1.689.073-4.948 0-3.259-.014-3.667-.072-4.947-.196-4.354-2.617-6.78-6.979-6.98-1.281-.059-1.69-.073-4.949-.073zm0 5.838c-3.403 0-6.162 2.759-6.162 6.162s2.759 6.163 6.162 6.163 6.162-2.759 6.162-6.163c0-3.403-2.759-6.162-6.162-6.162zm0 10.162c-2.209 0-4-1.79-4-4 0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.21-1.791 4-4 4zm6.406-11.845c-.796 0-1.441.645-1.441 1.44s.645 1.44 1.441 1.44c.795 0 1.439-.645 1.439-1.44s-.644-1.44-1.439-1.44z"></path>
			<?php
			break;
	}
	?>
	</svg>
	<?php
}

function harbinger_camel_case( string $string ){
	$string = str_replace( '-', ' ', $string );
	$string = str_replace( '_', ' ', $string );
	$string = ucwords( strtolower( $string ) );
	$string = str_replace( ' ', '', $string );
	return $string;
}

function harbinger_pascal_case( string $string ){
	$string = ucfirst( harbinger_camel_case( $string ) );
	return $string;
}
