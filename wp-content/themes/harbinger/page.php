<?php get_header( null, [ 'template' => 'default' ] ); ?>

<main>
    <?php
    while ( have_posts() ) :
        the_post();
        ?>

        <article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="container">
                <?php the_title( '<h1 class="page-title" itemprop="name">', '</h1>' ); ?>
            </header>
            <div class="page--default__main-content" itemprop="mainContentOfPage">
                <?php
                if ( has_post_thumbnail() ) {
                    the_post_thumbnail( 'full', [ 'itemprop' => 'image' ] );
                }
                the_content();
                ?>
            </div>
        </article>
    <?php endwhile ?>
</main>

<aside>
    <?php get_template_part( 'parts/start-project-cta' ); ?>
</aside>

<?php get_footer( null, [ 'template' => 'default' ] ); ?>
