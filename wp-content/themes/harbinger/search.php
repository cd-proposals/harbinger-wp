<?php get_header( null, [ 'template' => 'search' ] ) ?>

<main>
    <div class="container">
        <?php if ( have_posts() ) : ?>

            <header>
                <h1 class="page--search__page-title" itemprop="name"><?php echo esc_html__( 'Search results for:' ) ?> <strong><?php echo esc_html( get_search_query() ) ?></strong></h1>
            </header>
            <div class="page--search__page-content page--search__results">
                <?php
                while ( have_posts() ) :
                    the_post();
                    ?>
                    <article class="page--search__result-item">
                        <div itemprop="description" class="page--search__result-desc"><?php the_excerpt(); ?></div>
                        <div class="page--search__result-link"><?php wp_link_pages(); ?></div>
                    </article>
                <?php endwhile; ?>
            </div>

        <?php else : ?>

            <header>
                <h1 class="page--search__page-title" itemprop="name"><?php echo esc_html__( 'No results found for:' ) ?> <strong><?php echo esc_html( get_search_query() ) ?></strong></h1>
            </header>
            <div class="page--search__page-content">
                <p><?php esc_html_e( 'Sorry, nothing matched your search. Please try again.', 'harbinger' ); ?></p>
                <?php get_search_form(); ?>
            </div>

        <?php endif; ?>
    </div>
</main>

<?php get_footer( null, [ 'template' => 'search' ] ) ?>
