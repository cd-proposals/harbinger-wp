<?php
$template       = $args['template'];
$template_class = $template ? 'page--' . $template : '';
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> <?php harbinger_schema_type(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width" />
		<link rel="preconnect" href="https://fonts.googleapis.com" />
		<link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="anonymous" />
		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>
		<?php wp_body_open(); ?>
		<div id="site-wrapper" class="<?php echo harbinger_class_names( 'layout--primary', $template_class ) ?>">
			<?php get_template_part( 'parts/site-header' ); ?>
			<div tabindex="-1" id="<?php echo harbinger_get_top_link_anchor_id() ?>"></div>
			<div class="layout--primary__main">
