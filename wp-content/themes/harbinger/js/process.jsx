import throttle from "./vendor/lodash-es/throttle";
import observeRect from "./vendor/observe-rect";
import cx from "./vendor/clsx";
import {
  useCallback,
  useContext,
  useEffect,
  useLayoutEffect,
  useRef,
  useState,
} from "./vendor/preact-hooks";
import { h, createContext, render } from "./vendor/preact";
import { documentReady } from "./utils";

function useMediaQuery(query, defaultValue) {
  let [matches, setMatches] = useState(defaultValue || false);
  useEffect(() => {
    let mql = window.matchMedia(query);
    let handler = (event) => setMatches(event.matches);
    setMatches(mql.matches);
    mql.addEventListener("change", handler);
    return () => {
      mql.removeEventListener("change", handler);
    };
  }, [query]);
  return matches;
}

function useRect(nodeRef, options = {}) {
  let { observe, onChange } = options;
  let [element, setElement] = useState(nodeRef.current);
  let initialRectIsSet = useRef(false);
  let initialRefIsSet = useRef(false);
  let [rect, setRect] = useState(null);
  let onChangeRef = useRef(onChange);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useLayoutEffect(() => {
    onChangeRef.current = onChange;
    if (nodeRef.current !== element) {
      setElement(nodeRef.current);
    }
  });
  useLayoutEffect(() => {
    if (element && !initialRectIsSet.current) {
      initialRectIsSet.current = true;
      setRect(element.getBoundingClientRect());
    }
  }, [element]);
  useLayoutEffect(() => {
    if (!observe) {
      return;
    }
    let elem = element;
    // State initializes before refs are placed, meaning the element state will
    // be undefined on the first render. We still want the rect on the first
    // render, so initially we'll use the nodeRef that was passed instead of
    // state for our measurements.
    if (!initialRefIsSet.current) {
      initialRefIsSet.current = true;
      elem = nodeRef.current;
    }
    if (!elem) {
      return;
    }
    let observer = observeRect(elem, (rect) => {
      var _a;
      (_a = onChangeRef.current) === null || _a === void 0
        ? void 0
        : _a.call(onChangeRef, rect);
      setRect(rect);
    });
    observer.observe();
    return () => {
      observer.unobserve();
    };
  }, [observe, element, nodeRef]);
  return rect;
}

////////////////////////////////////////////////////////////////////////////////
// process-steps

let SectionContext = createContext(null);
function SectionProcess(props) {
  let { currentSection, registerHeading, unregisterHeading } =
    useSectionHeadings();
  return (
    <div className={cx(props.className, "ui--section-process")}>
      <div className="container ui--section-process__inner">
        <SectionContext.Provider value={{ registerHeading, unregisterHeading }}>
          {props.blocks.map((block, idx) => {
            return (
              <Block
                key={block.stepTitle}
                index={idx}
                highlight={currentSection === block.stepTitle}
                {...block}
              />
            );
          })}
        </SectionContext.Provider>
      </div>
    </div>
  );
}

function Block(props) {
  let ref = useRef(null);
  let rect = useRect(ref, { observe: true });
  let top = rect === null || rect === void 0 ? void 0 : rect.top;
  let { registerHeading, unregisterHeading } = useContext(SectionContext);
  useEffect(() => {
    if (top !== undefined) {
      registerHeading(props.stepTitle, top);
    }
    return () => {
      unregisterHeading(props.stepTitle);
    };
  }, [registerHeading, unregisterHeading, props.stepTitle, top]);
  return (
    <div key={props.stepTitle} className="ui--section-process__block">
      <div className="ui--section-process__block-heading-wrap">
        <h2
          ref={ref}
          className={cx("ui--section-process__block-heading", {
            ["ui--section-process__block-heading--active"]: props.highlight,
          })}
        >
          {props.stepTitle}
        </h2>
      </div>
      <div className="ui--section-process__steps">
        {props.subSteps.map((step) => {
          return (
            <div key={step.stepTitle} className="ui--section-process__step">
              <h3 className="ui--section-process__step-heading">
                {step.stepTitle}
              </h3>
              <div className="ui--section-process__step-body">
                {step.description
                  .trim()
                  .split("\n")
                  .map((l, i) => (
                    <p key={i}>{l.trim()}</p>
                  ))}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

function useSectionHeadings() {
  let shouldManageHeadings = useMediaQuery("screen and (min-width: 640px)");
  let [currentSection, setCurrentSection] = useState(null);
  let [headings, setHeadings] = useState([]);
  let registerHeading = useCallback((id, top) => {
    setHeadings((headings) => [
      ...headings.filter((h) => id !== h.id),
      { id, top },
    ]);
  }, []);
  let unregisterHeading = useCallback((id) => {
    setHeadings((headings) => headings.filter((h) => id !== h.id));
  }, []);
  // Doing a little ref magic here because we want a current reference to our
  // headings for our scroll listener, but we don't need to constantly set it up
  // and tear it down on every state change
  let sortedHeadingsRef = useRef(undefined);
  if (sortedHeadingsRef.current === undefined) {
    sortedHeadingsRef.current = sortHeadings(headings);
  }
  useEffect(() => {
    sortedHeadingsRef.current = sortHeadings(headings);
  });
  useEffect(() => {
    if (!shouldManageHeadings) {
      setCurrentSection(null);
      return;
    }
    function onScroll() {
      if (!sortedHeadingsRef.current.length) return;
      let y = window.pageYOffset;
      let windowHeight = window.innerHeight;
      if (y <= 0) {
        setCurrentSection(sortedHeadingsRef.current[0].id);
        return;
      }
      let middle = y + windowHeight / 2;
      let current = sortedHeadingsRef.current[0].id;
      for (let i = 0; i < sortedHeadingsRef.current.length; i++) {
        if (
          sortedHeadingsRef.current[i].top >= 0 &&
          middle >= sortedHeadingsRef.current[i].top
        ) {
          current = sortedHeadingsRef.current[i].id;
          break;
        }
      }
      setCurrentSection(current);
    }
    let throttled = throttle(onScroll, 100);
    window.addEventListener("scroll", throttled, {
      capture: true,
      passive: true,
    });
    window.requestAnimationFrame(() => {
      onScroll();
    });
    return () => {
      window.removeEventListener("scroll", throttled, true);
    };
  }, [shouldManageHeadings]);
  return { currentSection, registerHeading, unregisterHeading };
}

function sortHeadings(headings) {
  return headings.concat([]).sort((a, b) => a.top - b.top);
}

////////////////////////////////////////////////////////////////////////////////
// app

function App() {
  let blocks = window.__harbinger_process_blocks;
  return <SectionProcess blocks={blocks} />;
}

documentReady().then(() => {
  let rootId = "process-root";
  let rootElem = document.getElementById(rootId);
  if (rootElem) {
    render(<App />, rootElem);
  } else {
    console.error(
      `Could not find \`#${rootId}\`. This page will not render without an element with this id.`
    );
  }
});
