/**  @return {Promise<void>} */
export async function documentReady() {
  if (document.readyState != "loading") {
    return Promise.resolve();
  } else {
    return new Promise((resolve) => {
      document.addEventListener("DOMContentLoaded", resolve);
    });
  }
}

/**
 * @param {ParentNode | string} rootOrSelector
 * @param {string} [selector]
 * @returns {HTMLElement}
 */
export function $(rootOrSelector, selector) {
  let root = document;
  if (typeof selector === "string") {
    root = rootOrSelector instanceof Element ? rootOrSelector : root;
    return root.querySelector(selector);
  } else if (typeof rootOrSelector === "string") {
    return root.querySelector(rootOrSelector);
  }
  return null;
}

/**
 * @param {ParentNode | string} rootOrSelector
 * @param {string} [selector]
 * @returns {NodeListOf<HTMLElement>}
 */
export function $$(rootOrSelector, selector) {
  let root = document;
  if (typeof selector === "string") {
    root = rootOrSelector instanceof Element ? rootOrSelector : root;
    return root.querySelectorAll(selector);
  } else if (typeof rootOrSelector === "string") {
    return root.querySelectorAll(rootOrSelector);
  }
  return null;
}

/**
 * @param {Node} elementA
 * @param {Node} elementB
 * @returns {boolean}
 */
export function isElementPreceding(elementA, elementB) {
  return Boolean(
    elementB.compareDocumentPosition(elementA) &
      Node.DOCUMENT_POSITION_PRECEDING
  );
}

/**
 * @param {HTMLElement} elem
 * @param {NodeListOf<HTMLElement>} siblings
 * @returns {number}
 */
export function getIndexFromDOM(elem, siblings) {
  if (!elem) {
    return -1;
  }

  if (siblings.length === 0) {
    return 0;
  }

  let length = siblings.length;
  while (length--) {
    let index = length;
    let currentElem = siblings[index];
    if (!currentElem) {
      continue;
    }
    if (isElementPreceding(currentElem, elem)) {
      return index + 1;
    }
  }
  return 0;
}

/**
 * @param {keyof Console} type
 * @param {any[]} args
 */
export function log(type, ...args) {
  if (typeof console[type] !== "function" || args.length === 0) {
    args.unshift(type);
    type = "log";
  }
  let prefixStyle = "font-weight: bold";
  let prefix = "[harbinger-theme-scripts]";
  console[type]("%c" + prefix, prefixStyle, ...args);
}

/**
 * @param {string} name
 * @param {() => void} initFn
 */
export async function initUi(name, initFn) {
  await documentReady();
  try {
    initFn();
  } catch (e) {
    let prefixLength = 28;
    let message = "Failed to initialize " + name;
    let sep = "-".repeat(prefixLength + message.length);
    log("error", message + "\n" + sep);
    console.error(e);
  }
}
