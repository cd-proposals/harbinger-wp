import { A11yDialog } from "./vendor/a11y-dialog";
import { $, initUi } from "./utils";

/////////////////////////////////////////////////////////////////////////////
// main

Promise.all([
  // Always init UI in parallel unless dealing w/ UI dependencies
  initUi("site-nav", initSiteNav),
]);

/////////////////////////////////////////////////////////////////////////////
// site-nav

function initSiteNav() {
  let siteNavElems = {
    root: $(".ui--site-nav"),
    inner: $(".ui--site-nav__inner"),
    dialog: $("#site-nav-dialog"),
    dialogMount: $("#site-wrapper"),
    openTrigger: $("#site-nav-open-trigger"),
    closeTrigger: $("#site-nav-close-trigger"),
  };

  let siteNavDialog = new A11yDialog(
    siteNavElems.dialog,
    siteNavElems.dialogMount
  );

  let scrollY = window.scrollY;
  siteNavDialog.on("show", (dialogElem, triggerElem) => {
    scrollY = window.scrollY;
    document.body.style.overflow = "hidden";
    siteNavElems.root.dataset.state = "open";
    siteNavElems.inner.dataset.state = "open";
    siteNavElems.openTrigger.dataset.active = "";
    siteNavElems.closeTrigger.dataset.active = "";
    requestAnimationFrame(() => {
      window.scrollTo(0, scrollY);
    });
  });
  siteNavDialog.on("hide", (dialogEl, triggerEl) => {
    document.body.style.overflow = null;
    siteNavElems.root.dataset.state = "closed";
    siteNavElems.inner.dataset.state = "closed";
    delete siteNavElems.openTrigger.dataset.active;
    delete siteNavElems.closeTrigger.dataset.active;
    requestAnimationFrame(() => {
      window.scrollTo(0, scrollY);
    });
  });
}
