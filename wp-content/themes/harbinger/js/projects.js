import { $, $$, getIndexFromDOM, log, initUi } from "./utils";

/////////////////////////////////////////////////////////////////////////////
// constants
const KEY_ARROW_DOWN = "ArrowDown";
const KEY_ARROW_UP = "ArrowUp";
const KEY_ARROW_LEFT = "ArrowLeft";
const KEY_ARROW_RIGHT = "ArrowRight";
const KEY_PAGE_UP = "PageUp";
const KEY_PAGE_DOWN = "PageDown";
const KEY_END = "End";
const KEY_HOME = "Home";

const SELECTOR_TABS_ROOT = "[data-ui-tabs]";
const SELECTOR_TABS_TAB_LIST = "[data-ui-tab-list]";
const SELECTOR_TABS_TAB = "[data-ui-tab]";
const SELECTOR_TABS_TAB_PANEL_LIST = "[data-ui-tab-panels]";
const SELECTOR_TABS_TAB_PANEL = "[data-ui-tab-panel]";

/////////////////////////////////////////////////////////////////////////////
// main

Promise.all([
  // Always init UI in parallel unless dealing w/ UI dependencies
  initUi("tabs", initTabs),
]);

function initTabs() {
  let tabsRootElems = $$(SELECTOR_TABS_ROOT);
  for (let tabsRootElem of tabsRootElems) {
    initialize(tabsRootElem);
  }

  /** @param {HTMLElement} root */
  function initialize(root) {
    let tabElems = {
      tabList: $(root, SELECTOR_TABS_TAB_LIST),
      tabs: $$(root, SELECTOR_TABS_TAB),
      tabPanelList: $(root, SELECTOR_TABS_TAB_PANEL_LIST),
      tabPanels: $$(root, SELECTOR_TABS_TAB_PANEL),
    };

    // ensures initially active tab and panel are setup correctly
    initActiveTab();

    function initActiveTab() {
      let activeTab = Array.from(tabElems.tabs).find(isActiveTab);
      activateTab(activeTab);
    }

    /** @param {HTMLElement} tab */
    function getTabPanel(tab) {
      let panelId = tab.getAttribute("aria-controls");
      if (!panelId) {
        return null;
      }
      return (
        $(tabElems.tabPanelList, `${SELECTOR_TABS_TAB_PANEL}#${panelId}`) ||
        null
      );
    }

    /** @param {HTMLElement} panel */
    function getTab(panel) {
      let tabId = panel.getAttribute("aria-labelledby");
      if (!tabId) {
        return null;
      }
      return $(tabElems.tabList, `${SELECTOR_TABS_TAB}#${tabId}`) || null;
    }

    /** @param {HTMLElement} tab */
    function isActiveTab(tab) {
      return tab.dataset.selected != null;
    }

    /** @param {HTMLElement} tabToActivate */
    function activateTab(tabToActivate) {
      let panelToActivate = getTabPanel(tabToActivate);
      if (!panelToActivate) {
        log("warn", "No panel found for tab", tabToActivate);
      }

      for (let tab of tabElems.tabs) {
        if (tab === tabToActivate) {
          tab.dataset.selected = "";
          tab.tabIndex = 0;
          tab.setAttribute("aria-selected", "true");
        } else {
          if (tab.dataset.selected != null) {
            delete tab.dataset.selected;
          }
          tab.setAttribute("aria-selected", "false");
          tab.tabIndex = -1;
        }
      }
      for (let panel of tabElems.tabPanels) {
        if (panel === panelToActivate) {
          panel.dataset.selected = "";
          panel.hidden = false;
        } else {
          if (panel.dataset.selected != null) {
            delete panel.dataset.selected;
          }
          panel.hidden = true;
        }
      }
    }

    /** @param {MouseEvent | FocusEvent | KeyboardEvent} event */
    function getTriggeredTab(event) {
      /** @type {HTMLElement} */
      let target = event.target;
      /** @type {HTMLElement | null} */
      let trigger = target.closest(SELECTOR_TABS_TAB);
      return trigger || null;
    }

    /** @param {MouseEvent | FocusEvent} event */
    function activateTabOn(event) {
      let tab = getTriggeredTab(event);
      if (!tab) {
        return;
      }
      activateTab(tab);
    }

    /** @param {number} index */
    function getTabByIndex(index) {
      return $(tabElems.tabList, `${SELECTOR_TABS_TAB}[data-index="${index}"]`);
    }

    tabElems.tabs.forEach((tab) => {
      let index = getIndexFromDOM(tab, tabElems.tabs);
      tab.dataset.index = index;
    });

    tabElems.tabList.addEventListener("click", activateTabOn);
    tabElems.tabList.addEventListener("focusin", activateTabOn);
    tabElems.tabList.addEventListener("keydown", (event) => {
      if (
        ![
          KEY_ARROW_DOWN,
          KEY_ARROW_UP,
          KEY_ARROW_LEFT,
          KEY_ARROW_RIGHT,
          KEY_PAGE_UP,
          KEY_PAGE_DOWN,
          KEY_END,
          KEY_HOME,
        ].includes(event.key)
      ) {
        return;
      }

      let tab = getTriggeredTab(event);
      if (!tab) {
        return;
      }

      let panel = getTabPanel(tab);
      let index = parseInt(tab.dataset.index, 10);
      let next = getTabByIndex(index + 1);
      let prev = getTabByIndex(index - 1);
      let first = getTabByIndex(0);
      let last = getTabByIndex(tabElems.tabs.length - 1);
      switch (event.key) {
        case KEY_ARROW_DOWN: {
          if (panel) {
            event.preventDefault();
            panel.focus();
          }
          break;
        }
        case KEY_ARROW_RIGHT: {
          event.preventDefault();
          next ? next.focus() : first ? first.focus() : void 0;
          break;
        }
        case KEY_ARROW_LEFT: {
          event.preventDefault();
          prev ? prev.focus() : last ? last.focus() : void 0;
          break;
        }
        case KEY_PAGE_UP: {
          event.preventDefault();
          event.ctrlKey && prev ? prev.focus() : first ? first.focus() : void 0;
        }
        case KEY_PAGE_DOWN: {
          event.preventDefault();
          event.ctrlKey && next ? next.focus() : last ? last.focus() : void 0;
        }
        case KEY_HOME: {
          event.preventDefault();
          first ? first.focus() : void 0;
        }
        case KEY_END: {
          event.preventDefault();
          last ? last.focus() : void 0;
        }
      }
    });
  }
}
