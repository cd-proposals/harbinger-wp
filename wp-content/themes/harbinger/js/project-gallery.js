import { Fancybox } from "./vendor/fancybox";
import { initUi } from "./utils";

Promise.all([
  // Always init UI in parallel unless dealing w/ UI dependencies
  initUi("fancybox", initFancybox),
]);

function initFancybox() {
  Fancybox.bind('[data-fancybox="gallery"]', {
    dragToClose: false,
    Toolbar: false,
    closeButton: "top",
    Image: {
      zoom: false,
    },
    on: {
      initCarousel: (fancybox) => {
        let slide = fancybox.Carousel.slides[fancybox.Carousel.page];
        fancybox.$container.style.setProperty(
          "--bg-image",
          `url("${slide.$thumb.src}")`
        );
      },
      "Carousel.change": (fancybox, carousel, to, from) => {
        let slide = carousel.slides[to];
        fancybox.$container.style.setProperty(
          "--bg-image",
          `url("${slide.$thumb.src}")`
        );
      },
    },
  });
}
