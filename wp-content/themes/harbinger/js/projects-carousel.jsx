import throttle from "./vendor/lodash-es/throttle";
import debounce from "./vendor/lodash-es/debounce";
import cx from "./vendor/clsx";
import {
  useState,
  useEffect,
  useCallback,
  useLayoutEffect,
  useRef,
} from "./vendor/preact-hooks";
import { h, render } from "./vendor/preact";
import { documentReady } from "./utils";

////////////////////////////////////////////////////////////////////////////////
// utils

/**
 * @param {{
 *   activeProjectIndex: number;
 *   projects: Project[];
 *   position: 'left' | 'prev' | 'center' | 'next' | 'right';
 * }} args
 * @return {ProjectSlide}
 */
function getSlide({ activeProjectIndex, projects, position }) {
  let idx = -1;
  switch (position) {
    case "left":
      idx = Math.max(
        activeProjectIndex === 1
          ? projects.length - 1
          : activeProjectIndex === 0
          ? projects.length - 2
          : activeProjectIndex - 2,
        0
      );
      break;
    case "prev":
      idx = Math.max(
        activeProjectIndex === 0 ? projects.length - 1 : activeProjectIndex - 1,
        0
      );
      break;
    case "center":
      idx = activeProjectIndex;
      break;
    case "next":
      idx = (activeProjectIndex + 1) % projects.length;
      break;
    case "right":
      idx = (activeProjectIndex + 2) % projects.length;
      break;
  }

  return {
    _idx: idx,
    position,
    get project() {
      return projects[this._idx];
    },
    get isActive() {
      return this.position === "center";
    },
  };
}

function getMatrixFactor(windowWidth, windowHeight) {
  let isLandscape = windowWidth > windowHeight;
  let factor = windowWidth > 1024 && isLandscape ? 0.394 : 0.4612;
  let amount = factor * windowWidth;
  return amount;
}

function usePrevious(value) {
  const ref = useRef(null);
  useEffect(() => {
    ref.current = value;
  }, [value]);
  return ref.current;
}

////////////////////////////////////////////////////////////////////////////////
// projects-carousel

/**
 * @param {{ projects: Project[]; className?: string }} props
 */
function ProjectsCarousel({ projects, className }) {
  if (projects.length < 1) {
    throw Error("No projects in the carousel");
  }

  let [transition, setTransition] = useState("idle");
  let [activeProject, setActiveProject] = useState(projects[0]);
  let [shouldTransition, setShouldTransition] = useState(false);
  let [mtxFactor, setMtxFactor] = useState(0);
  let [mouseInSlides, setMouseInSlides] = useState(false);

  useEffect(() => {
    setMtxFactor(getMatrixFactor(window.innerWidth, window.innerHeight));
    let listener = debounce((event) => {
      let win = event.target;
      setMtxFactor(getMatrixFactor(win.innerWidth, win.innerHeight));
    }, 500);
    window.addEventListener("resize", listener);
    return () => {
      window.removeEventListener("resize", listener);
    };
  }, []);

  let activeProjectIndex = projects.findIndex(
    (project) => project.id === activeProject.id
  );

  let slides = ["left", "prev", "center", "next", "right"].map((position) =>
    getSlide({ position, activeProjectIndex, projects })
  );

  let previousTransition = usePrevious(transition);
  let previousScrollPositionRef = useRef(-1);

  useEffect(() => {
    if (previousTransition == null || previousTransition === transition) {
      return;
    }
    if (transition === "idle") {
      setActiveProject((activeProject) => {
        let activeIndex = projects.findIndex(
          (project) => project.id === activeProject.id
        );
        let nextIndex = (activeIndex + 1) % projects.length;
        let prevIndex = (activeIndex + projects.length - 1) % projects.length;
        if (previousTransition === "backward") {
          return projects[prevIndex];
        }
        if (previousTransition === "forward") {
          return projects[nextIndex];
        }
        return activeProject;
      });
    }
  }, [transition, previousTransition, projects]);

  // Because we are adding/removing elements out of view, some browsers will
  // change the scroll position after the animation is complete. In the
  // element's onAnimationStart handler we kick off a function that updates the
  // scroll position on every tick and tracks it in the prevScrollPositionRef.
  // Here we just wait for an additional paint and then move the scroll position
  // to wherever it was on the last animation frame to prevent any jumpiness.
  useLayoutEffect(() => {
    if (previousScrollPositionRef.current === -1) return;
    window.scrollTo(0, previousScrollPositionRef.current);
  }, [activeProjectIndex]);

  let slidesRef = useRef(null);
  let mouseInCenterRef = useRef(false);
  let getSlidesListener = useCallback(() => {
    return (event) => {
      let deltaX = Number(
        ((event.clientX / window.innerWidth - 0.5) * 20).toFixed(2)
      );
      let deltaY = Number(
        ((event.clientY / window.innerHeight - 0.5) * 20).toFixed(2)
      );
      let captionTransformX = `${deltaX}px`;
      let captionTransformY = `${deltaY}px`;
      let imageTransformX = `${deltaX * -1}px`;
      let imageTransformY = `${deltaY * -1}px`;
      for (let i = 0; i < slideImagesRef.current.length; i++) {
        let slideCaption = slideCaptionsRef.current[i];
        let slideImage = slideImagesRef.current[i];
        let isCenterSlide =
          slideImage.getAttribute("data-position") === "center";
        let translation = `${
          isCenterSlide && mouseInCenterRef.current
            ? "scale(1.57)"
            : "scale(1.5)"
        } translate(${imageTransformX}, ${imageTransformY})`;
        slideImage.style.transform = translation;
        slideCaption.style.transform = `translate(calc(-50% + ${captionTransformX}), calc(-50% + ${captionTransformY})) rotate(calc(var(--rotation-angle) * 1.5))`;
      }
    };
  }, []);
  let slideImagesRef = useRef([]);
  let slideCaptionsRef = useRef([]);

  useEffect(() => {
    let slidesContainer = slidesRef.current;
    let DEBUONCE = 20;
    let transformSlides = throttle(getSlidesListener(), DEBUONCE);
    function removeTransforms() {
      for (let i = 0; i < slideImagesRef.current.length; i++) {
        let slideImage = slideImagesRef.current[i];
        let slideCaption = slideCaptionsRef.current[i];
        setTimeout(() => {
          slideImage.style.transform = "";
          slideCaption.style.transform = "";
        }, DEBUONCE + 10);
      }
    }
    const eventType = "mousemove";
    if (mouseInSlides && transition === "idle") {
      slidesContainer === null || slidesContainer === void 0
        ? void 0
        : slidesContainer.addEventListener(eventType, transformSlides);
    } else {
      slidesContainer === null || slidesContainer === void 0
        ? void 0
        : slidesContainer.removeEventListener(eventType, transformSlides);
      removeTransforms();
    }
    return () => {
      slidesContainer === null || slidesContainer === void 0
        ? void 0
        : slidesContainer.removeEventListener(eventType, transformSlides);
      removeTransforms();
    };
  }, [getSlidesListener, transition, mouseInSlides]);
  let transitionRef = useRef(transition);
  useLayoutEffect(() => {
    transitionRef.current = transition;
  }, [transition]);
  return (
    <div data-ui-carousel="" className={cx(className, "ui--projects-carousel")}>
      <div
        className="ui--projects-carousel__wrap"
        // Helps w/ debugging out-of-view slides
        // style={{
        //   transform: "translateX(70%)",
        //   width: "40%",
        // }}
      >
        <div className="ui--projects-carousel__nav-button-wrap">
          <KeyboardNavButton
            direction="prev"
            onClick={() => {
              setShouldTransition(true);
              window.requestAnimationFrame(() => {
                setTransition("backward");
              });
            }}
          >
            <span className="sr-only">Previous slide</span>
          </KeyboardNavButton>
          <KeyboardNavButton
            direction="next"
            onClick={() => {
              setShouldTransition(true);
              window.requestAnimationFrame(() => {
                setTransition("forward");
              });
            }}
          >
            <span className="sr-only">Next slide</span>
          </KeyboardNavButton>
        </div>
        <div
          ref={slidesRef}
          data-ui-carousel-slides=""
          className={cx("ui--projects-carousel__slides", {
            "ui--projects-carousel__slides--animate": shouldTransition,
          })}
          style={{
            // @ts-ignore
            "--mtx-factor": mtxFactor,
          }}
          onMouseEnter={() => {
            setMouseInSlides(true);
          }}
          onAnimationStart={() => {
            document.body.parentElement.style.scrollBehavior = "auto";
            function tick() {
              previousScrollPositionRef.current = window.scrollY;
              if (transitionRef.current !== "idle") {
                requestAnimationFrame(tick);
              }
            }
            tick();
          }}
          onAnimationEnd={() => {
            if (transition !== "idle") {
              setTransition("idle");
              window.requestAnimationFrame(() => {
                setShouldTransition(false);
              });
            }
          }}
        >
          {slides.map((slideData, index) => {
            let { isActive, project, position } = slideData;
            let title = project.clientName;
            let LinkComp = isActive ? "a" : "div";
            let linkTo = `/work/${project.slug}`;
            let linkLabel = `See project: ${project.projectName}`;
            let linkProps = isActive
              ? { href: linkTo, "aria-label": linkLabel }
              : {};

            let isTransitioningOutOfCenter =
              position === "center" && transition !== "idle";
            let isTransitioningToCenter =
              (position === "prev" && transition === "backward") ||
              (position === "next" && transition === "forward");

            return (
              <figure
                key={slideData.project.id + "-" + index}
                data-active={isActive}
                data-ui-carousel-slide=""
                onMouseEnter={() => {
                  if (position === "center") {
                    mouseInCenterRef.current = true;
                  }
                }}
                onMouseLeave={() => {
                  mouseInCenterRef.current = false;
                }}
                onClick={() => {
                  if (transition !== "idle") {
                    return;
                  }
                  if (position === "prev") {
                    setShouldTransition(true);
                    window.requestAnimationFrame(() => {
                      setTransition("backward");
                    });
                  }
                  if (position === "next") {
                    setShouldTransition(true);
                    window.requestAnimationFrame(() => {
                      setTransition("forward");
                    });
                  }
                }}
                className={cx(
                  "ui--projects-carousel__slide",
                  `ui--projects-carousel__slide--transition-${transition}`,
                  {
                    [`ui--projects-carousel__slide--${position}`]: position,
                    "ui--projects-carousel__slide--animate": shouldTransition,
                  }
                )}
              >
                <LinkComp
                  className="ui--projects-carousel__slide-link"
                  {...linkProps}
                >
                  <SlideImage
                    {...{ index, transition, shouldTransition, slideImagesRef }}
                    {...slideData}
                    isTransitioningOutOfCenter={isTransitioningOutOfCenter}
                    isTransitioningToCenter={isTransitioningToCenter}
                  />
                </LinkComp>
                <SlideCaption index={index} slideCaptionsRef={slideCaptionsRef}>
                  {title}
                </SlideCaption>
              </figure>
            );
          })}
        </div>
      </div>
    </div>
  );
}

function SlideImage({
  project,
  index,
  position,
  slideImagesRef,
  transition,
  isActive,
  isTransitioningToCenter,
  isTransitioningOutOfCenter,
  shouldTransition,
}) {
  const centerState =
    transition === "idle" && position === "center"
      ? "idle"
      : isTransitioningToCenter
      ? "entering"
      : isTransitioningOutOfCenter
      ? "exiting"
      : undefined;

  var _a;
  let imageRef = useCallback(
    (node) => {
      if (node) slideImagesRef.current[index] = node;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [index]
  );
  let image = null;

  if (
    (_a = project.featuredImage) === null || _a === void 0 ? void 0 : _a.sizes
  ) {
    let { featuredImage } = project;
    image = Object.keys(featuredImage.sizes).reduce((prev, size) => {
      let image = featuredImage.sizes[size];
      if (!image) return prev;
      let which = prev && prev.height > image.height ? prev : image;
      return Object.assign(Object.assign({}, which), {
        alt: featuredImage.alt,
      });
    }, null);
  } else {
    // TODO: Remove this once we have a default image
    image = {
      alt: "PLACEHOLDER",
      height: 500,
      width: 500,
      url: "https://via.placeholder.com/500x500",
    };
  }
  return (
    <div className="ui--projects-carousel__slide-image-wrapper">
      <div
        ref={imageRef}
        data-center-state={centerState}
        data-transition={transition}
        data-position={position}
        data-entering-center={isTransitioningToCenter ? "" : undefined}
        data-exiting-center={isTransitioningOutOfCenter ? "" : undefined}
        data-ui-carousel-slide-image=""
        className={cx("ui--projects-carousel__slide-image", {
          "ui--projects-carousel__slide-image--animate": shouldTransition,
        })}
        role="img"
        aria-label={`Design for ${project.title}`}
        style={{
          backgroundImage: (
            image === null || image === void 0 ? void 0 : image.url
          )
            ? `url('${image.url}')`
            : undefined,
          opacity:
            (transition === "idle" && isActive) ||
            (transition === "backward" && position === "prev") ||
            (transition === "forward" && position === "next")
              ? 1
              : 0.75,
          pointerEvents:
            transition !== "idle" || position === "left" || position === "right"
              ? "none"
              : undefined,
          zIndex: 1,
        }}
      ></div>
    </div>
  );
}

function SlideCaption({ index, slideCaptionsRef, children }) {
  let captionRef = useCallback(
    (node) => {
      if (node) slideCaptionsRef.current[index] = node;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [index]
  );
  return (
    <figcaption
      ref={captionRef}
      className="ui--projects-carousel__slide-caption"
      data-ui-carousel-slide-caption=""
    >
      {children}
    </figcaption>
  );
}

function KeyboardNavButton({ direction, ...props }) {
  let ref = useRef(null);
  let [hasFocus, setHasFocus] = useState(false);
  useEffect(() => {
    setHasFocus(document.activeElement === ref.current);
  }, []);
  return (
    <button
      ref={ref}
      onFocus={() => setHasFocus(true)}
      onBlur={() => setHasFocus(false)}
      {...props}
      className={cx(
        props.className,
        "ui--projects-carousel__nav-button",
        `ui--projects-carousel__nav-button--${direction}`
      )}
      data-focused={hasFocus ? "" : undefined}
      data-ui-carousel-nav-button=""
      {...{ [`data-ui-carousel-nav-button-${direction}`]: "" }}
    />
  );
}

////////////////////////////////////////////////////////////////////////////////
// app

function App() {
  let projects = window.__harbinger_projects;
  if (!projects || !Array.isArray(projects) || projects.length < 1) {
    console.error("No projects in the carousel");
    return null;
  }
  return (
    <ProjectsCarousel
      className="page--home__projects-carousel"
      projects={projects}
    />
  );
}

// parts/projects-carousel.php
documentReady().then(() => {
  let rootId = "projects-carousel";
  let rootElem = document.getElementById(rootId);
  if (rootElem) {
    render(<App />, rootElem);
  } else {
    console.error(
      `Could not find \`#${rootId}\`. This page will not render without an element with this id.`
    );
  }
});

/**
 * @typedef {"forward" | "backward" | "idle"} SlideTransition
 *
 * @typedef {{
 *   id: number;
 *   alt?: string | null;
 *   caption?: string | null;
 *   sizes: Record<string, { url: string; width: number; height: number }>;
 * }} WPImage
 *
 * @typedef {{
 *   image?: WPImage | null
 *   name: string;
 *   url: string;
 * }} ProjectVideo
 *
 * @typedef {{
 *   readonly isActive: boolean;
 *   readonly position: "left" | "prev" | "center" | "next" | "right";
 *   readonly project: Project;
 * }} ProjectSlide
 *
 * @typedef {{
 *   datePublished: string;
 *   guid: string;
 *   id: number;
 *   slug: string;
 *   status: "publish" | "draft";
 *   title: string;
 *   clientName?: Microsoft;
 *   projectName?: string;
 *   projectDescription?: string;
 *   videos?: ProjectVideo[] | null;
 *   credits?: { job: string; credit: string }[] | null;
 *   featuredImage?: WPImage | null;
 * }} Project
 */
