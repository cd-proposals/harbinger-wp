			</div>
			<?php get_template_part( 'parts/site-footer', null, [ 'class' => 'layout--primary__footer' ] ); ?>
		</div>
		<?php get_template_part( 'parts/site-nav', null, [
			'class'              => 'site-header__nav',
			'close_button_class' => 'site-header__nav-button-close',
		] ); ?>
		<?php wp_footer(); ?>
	</body>
</html>
