<?php get_header( null, [ 'template' => '404' ] ) ?>

<main>
    <article class="container">
        <header>
            <h1 itemprop="name" class="page--404__page-title"><?php esc_html_e( 'Page not Found', 'harbinger' ); ?></h1>
        </header>
        <div class="page--404__page-content">
            <p><?php esc_html_e( 'Nothing found for the requested page.', 'harbinger' ); ?></p>
        </div>
    </article>
</main>

<?php get_footer( null, [ 'template' => '404' ] ) ?>
